Rails.application.routes.draw do
  resources :microposts

  resources :users
  root 'users#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  # root 'application#hello'
 
end
